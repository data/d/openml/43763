# OpenML dataset: Electoral-Integrity-in-2016-US-Election

https://www.openml.org/d/43763

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Electoral integrity refers to international standards and global norms governing the appropriate conduct of elections. These standards have been endorsed in a series of authoritative conventions, treaties, protocols, and guidelines by agencies of the international community and apply universally to all countries throughout the electoral cycle, including during the pre-electoral period, the campaign, on polling day, and in its aftermath.
Content
The Perceptions of Electoral Integrity (PEI) survey asks experts to evaluate elections according to 49 indicators, grouped into eleven categories reflecting the whole electoral cycle. The PEI dataset is designed to provide a comprehensive, systematic and reliable way to monitor the quality of elections worldwide. It includes disaggregated scores for each of the individual indicators, summary indices for the eleven dimensions of electoral integrity, and a PEI index score out of 100 to summarize the overall integrity of the election.
Acknowledgements
This study was conducted by Pippa Norris, Alessandro Nai, and Max Grmping for Harvard University's Electoral Integrity Project.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43763) of an [OpenML dataset](https://www.openml.org/d/43763). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43763/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43763/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43763/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

